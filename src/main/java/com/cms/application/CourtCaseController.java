package com.cms.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CourtCaseController {

    @Autowired
    private CourtCaseService courtCaseService;

    @GetMapping("/cases")
    public List<CourtCase> getAllCases() {
        return courtCaseService.getAllCases();
    }

    @GetMapping("/cases/{caseId}")
    public CourtCase getCase(@PathVariable(value="caseId") Long caseId) {
        return courtCaseService.getCase(caseId);
    }

    @PostMapping("/cases")
    public ResponseEntity<Void> createCase(@RequestBody CourtCase courtCase) {
        // New comment
        courtCaseService.saveCase(courtCase);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}
