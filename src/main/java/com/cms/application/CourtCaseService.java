package com.cms.application;

import com.cms.application.CourtCase;
import com.cms.application.CourtCaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CourtCaseService {

    @Autowired
    private CourtCaseRepository courtCaseRepository;

    public List<CourtCase> getAllCases() {
        return getListFromIteralbe(courtCaseRepository.findAll());
    }

    public CourtCase getCase(Long id) {
        return courtCaseRepository.findById(id).get();
    }

    public void saveCase(CourtCase courtCase) {
        courtCaseRepository.save(courtCase);
    }

    private <T> List<T> getListFromIteralbe(Iterable<T> itr)
    {
        List<T> cltn = new ArrayList<>();
        for (T t : itr)
            cltn.add(t);
        return cltn;
    }
}
