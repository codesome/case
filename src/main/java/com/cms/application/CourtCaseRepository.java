package com.cms.application;

import com.cms.application.CourtCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

public interface CourtCaseRepository extends CrudRepository<CourtCase, Long> {
    List<CourtCase> findByName(String name);
}
