package com.cms.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourtCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourtCaseApplication.class, args);
	}

}
